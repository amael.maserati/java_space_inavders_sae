public class Vie {
    /** Cet attribut modélise le nombre de vies du joueur */
    private int nbVies;
    /** permet de construire le nombre de vies du joueur
     * @param nbVies le nombre de vies du joueur
     */
    public Vie(int nbVies) {
        this.nbVies = nbVies;
    }
    /** permet d'actualiser le nombre de vies du joueur
     * @return le nombre de vies du joueur actualisé
     */
    public void setNbVies(int vie) {
        this.nbVies = vie;
    }
    /** permet de récupérer le nombre de vies du joueur
     * @return le nombre de vies du joueur
     */
    public int getNbVies() {
        return this.nbVies;
    }
}
