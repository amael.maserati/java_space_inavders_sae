import java.io.*;
import javax.sound.sampled.*;

public class Audio {
    /** Cet attribut modélise le son du tir du vaisseau */
    private Clip sonTirVaisseau;
    /** Cet attribut modélise le son du vaisseau touché par un projectile */
    private Clip sonVaisseauTouche;
    /** Cet attribut modélise le son de la soucoupe touchée par un projectile */
    private Clip sonSoucoupeTouche;
    /** Cet attribut modélise le son d'un alien touché par un projectile */
    private Clip sonAlienTouche;
    /** Permet de construire les sons */
    public Audio() {
        try {
            sonTirVaisseau = AudioSystem.getClip();
            sonTirVaisseau.open(AudioSystem.getAudioInputStream(new File("src/sons/sonTirVaisseau.wav")));
            sonVaisseauTouche = AudioSystem.getClip();
            sonVaisseauTouche.open(AudioSystem.getAudioInputStream(new File("src/sons/sonDestructionVaisseau.wav")));
            sonSoucoupeTouche = AudioSystem.getClip();
            sonSoucoupeTouche.open(AudioSystem.getAudioInputStream(new File("src/sons/sonDestructionSoucoupe.wav")));
            sonAlienTouche = AudioSystem.getClip();
            sonAlienTouche.open(AudioSystem.getAudioInputStream(new File("src/sons/sonAlienMeurt.wav")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /** permet d'émettre le son du tir du vaisseau */
    public void emettreSonTirVaisseau() {
        sonTirVaisseau.setFramePosition(0);
        sonTirVaisseau.start();
    }
    /** permet d'émettre le son du vaisseau touché */
    public void emettreSonVaisseauTouche() {
        sonVaisseauTouche.setFramePosition(0);
        sonVaisseauTouche.start();
    }
    /** permet d'émettre le son de la soucoupe touchée */
    public void emettreSonSoucoupeTouche() {
        sonSoucoupeTouche.setFramePosition(0);
        sonSoucoupeTouche.start();
    }
    /** permet d'émettre le son d'un alien touché */
    public void emettreSonAlienTouche() {
        sonAlienTouche.setFramePosition(0);
        sonAlienTouche.start();
    }
}
