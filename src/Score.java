public class Score {
    /** Cet attribut modélise le score du joueur */
    private int score;
    /** Permet de construire le score du joueur */
    public Score() {
        this.score = 0;
    }
    /** Permet d'obtenir le score du joueur
     * @return le score du joueur
     */
    public int getScore() {
        return this.score;
    }
    /** Permet d'ajouter des points au score du joueur
     * @param points les points à ajouter au score du joueur
     */
    public void ajoute(int points) {
        this.score += points;
    }
}
